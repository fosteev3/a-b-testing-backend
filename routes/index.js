const express = require('express');
const os = require('os');
const router = express.Router();
const Configuration = require('../configuration');
const {Pool, Client} = require('pg');

const config = new Configuration();

const pool = new Pool(config.getPostgresql());

router.get('/', (req, res) => {
    res.send('backend worked!');
});

router.get('/columns', async (req, res) => {
    const getRandomInt = (max) => Math.floor(Math.random() * Math.floor(max));

    const getSlice = arr => {
        const chunk = (arr.length / 10) / 2; // 5% in chunk
        return arr.sort((a,b) => a - b).slice(0 + chunk, arr.length - chunk);
    }

    const getArithmeticMean = values => (values.reduce((acc, cur) => acc + cur, 0) / values.length);


    /**
     * Здесь намерено вставил рандомный offset
     * из-за того что postgres кешерует запрос, и выдает один и тот же массив
     */

    const getColumnRandomValues = (column) => pool.query(`SELECT ${column} FROM values 
                                                          OFFSET ${getRandomInt(900)} LIMIT 100`);

    try {
        const client = await pool.connect();

        let response = [];

        for (let i = 0; i < 10; i++) {

            const column1 = await getColumnRandomValues('column1');
            const column2 = await getColumnRandomValues('column2');

            response.push({
                column1: getArithmeticMean(getSlice(column1.rows.map(r => r.column1))),
                column2: getArithmeticMean(getSlice(column2.rows.map(r => r.column2)))
            })
        }

        client.release();

        res.json(response);

    } catch (e) {
        res.status(500);
        res.send(e.message);
    }
});

router.get('/generate', async (req, res) => {
    try {
        const client = await pool.connect();

        const random = '(SELECT random() * 100)';

        for (let i = 0; i < 1000; i++) {
            await client.query(`INSERT INTO values  (column1, column2)                    
                                   VALUES (${random}, ${random})`);
        }

        client.release();

        res.send('Success generate');
    } catch (e) {
        res.send(e.message);
    }
});

module.exports = router;
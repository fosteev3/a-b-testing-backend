class Configuration {
    constructor() {
        const fs = require("fs");
        const jsonContent = JSON.parse(fs.readFileSync("configuration.json"));
        this.headers = jsonContent.headers;
        this.port = jsonContent.port;
        this.path  = jsonContent.path;
        this.postgres = jsonContent.postgres;
    }

    getHedaers() {
        return this.headers;
    }

    getPort() {
        return this.port;
    }

    getPath() {
        return this.path;
    }

    getPostgresql() {
        return this.postgres;
    }
}

module.exports = Configuration;